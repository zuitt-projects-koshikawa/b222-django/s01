from django.http import HttpResponse
from django.shortcuts import render

# from django.shortcuts import render
# 'from' keyword allows importing of necessary classes, methods and other items needed in our application.
# While 'import' keyword defines what we are  importing in the package.

# Create your views here.
def index(request):
    return HttpResponse("Hello from  the views.py file")
